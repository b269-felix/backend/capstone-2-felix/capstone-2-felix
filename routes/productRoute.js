const express = require("express");
// Creates a router instance that functions as a middleware and routing system
const router = express.Router();
// Route for checking if the user's email already exists in the database
const productController = require("../controllers/productController");
const User = require("../models/User");

const auth = require("../auth");

// route for creating a product 
router.post("/addProduct", auth.verify, (req, res) => {
	const item = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.createProduct(item).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all active products
router.get("/activeProduct", (req, res) => {
	productController.activeProducts().then(resultFromController => res.send(resultFromController));
});

// Route for getting all products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send (resultFromController));
});

// Route for updating a product
router.put("/:productId/update", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(data, req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for achivieng a product
router.patch("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archieveProduct(data, req.params).then(resultFromController => res.send(resultFromController));
});
















module.exports = router;