const express = require("express");
// Creates a router instance that functions as a middleware and routing system
const router = express.Router();
// Route for checking if the user's email already exists in the database
const userController = require("../controllers/userController");

const auth = require("../auth");

// Route for registering a new user / customer
router.post("/signup", (req, res) => {
	userController.registerCustomer(req.body).then(resultFromController => res.send(resultFromController));
});

// User/customer Authentication
router.post("/login", (req, res) => {
	userController.customerLogin(req.body).then(resultFromController => res.send(resultFromController));
});



module.exports = router;