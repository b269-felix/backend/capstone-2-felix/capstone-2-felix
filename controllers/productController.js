const Product = require("../models/Product");
const User = require("../models/User");

// Adding products
module.exports.createProduct = (item) => {
	if(item.isAdmin) {
		let newProduct = new Product ({
			name: item.product.name,
			description: item.product.description,
			price: item.product.price,
			availableStocks: item.product.availableStocks
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let notice = Promise.resolve('User must be an admin to access this');
	return notice.then((value) => {
		return {value};
	});
};

// Retrieve all active products
module.exports.activeProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

// Route for getting products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result =>{
		return result;
	});
};

// Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// Update product information 
module.exports.updateProduct = (data, reqParams, reqBody) => {
	if(data.isAdmin) {
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			availableStocks: reqBody.availableStocks
		};
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let notice = Promise.resolve('User must be an admin to access this');
	return notice.then((value) => {
		return {value};
	});

};
// Archive a non-active product
module.exports.archieveProduct = (data, reqParams) => {
	if(data.isAdmin) {
		let updateActiveField = {
			isActive: false
		};
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let notice = Promise.resolve('User must be an admin to access this');
	return notice.then((value) => {
		return {value};
	});
};




