// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Allows connection to userRoute 
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");

// Server
const app = express();

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Allows all the user routes created in "userRoute.js" file to use "./users" as route (resources)
app.use("/users", userRoute);
app.use("/products", productRoute);

// Database Connection
mongoose.connect("mongodb+srv://gioangelofelix2:admin123@zuitt-bootcamp.gwsbg1x.mongodb.net/E-commerceAPI?retryWrites=true&w=majority",
	{
		
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once("open", () => console.log('Now connected to cloud database!'));

app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`));



