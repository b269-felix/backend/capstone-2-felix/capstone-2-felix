const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product name is required"]
	},
	description : {
		type : String,
		required : [true, "Product description is required"]
	},
	price : {
		type : Number,
		required : [true, "Product Price is required"]
	},
	availableStocks : {
		type : Number,
		required : [true, "Number of Stock is required before adding a product!"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	orders : [{
		orderId : {
			type : String,
			required : [true, "Order ID is required"]
		}
	}]
});
module.exports = mongoose.model("Product", productSchema);
