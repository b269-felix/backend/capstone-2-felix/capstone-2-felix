const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required : [true, "First Name is required!"]
	},
	lastName : {
		type: String,
		required: [true, "Last Name is required!"]
	},
	email : {
		type: String,
		required: [true, "Email address is required!"]
	},
	password : {
		type: String,
		required: [true, "Password is required!"]	
	},
	mobileNo : {
		type: String,
		required : [true, "Mobile Number is required!"]
	},
	isAdmin : {
		type: Boolean,
		default: false		
	},
	orders : [{
		products : [{
			productName : {
				type : String,
				required: [true, "Product Name is required!"]
			},
			quantity : {
				type: Number,
				required : [true, "Number of orders is required!"]
			}
		}],
		totalAmount : {
			type : Number,
			required: true
		},
		purchasedOn : {
			type: Date,
			default : new Date()
		}
	}]
});
module.exports = mongoose.model("User", userSchema);